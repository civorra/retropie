#!/usr/bin/env bash

function abort() {
  printf "\nAn error occurred. Exiting...\n" >&2
  exit 1
}

trap 'abort' 0
set -e
# ------------------------------------------------------------------------------

printf "Using workspace: "
pwd

# ------------------------------------------------------------------------------
# Global variables
# ------------------------------------------------------------------------------
source7zFolder="7z"
outputFolder="psx"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------
function unZipGame() {
  game7zPath=$1
  outputGamePath=$2

  # Run 7z extraxt command
  7z e "$game7zPath" -oc:"$outputGamePath" "*.bin" -r

  printf "\nFile '$game7zPath' extracted in '$outputGamePath'\n"
}

function generateCueFile() {
  gameName=$1
  gamePath=$2

  # Get .bin files from folder
  gameBinArray=("$gamePath"/*.bin)

  # Generate cueFile content
  declare content=""
  declare -i trackNumber=1

  for binFile in "${gameBinArray[@]}"; do
    binName=$(basename "$binFile")
    trackstr=$(printf "%02d" $trackNumber)

    content="${content}FILE \"$binName\" BINARY\n"
    if [ $trackNumber -eq 1 ]; then
      content="${content}  TRACK $trackstr MODE2/2352\n"
      content="${content}    INDEX 01 00:00:00"
    else
      content="${content}  TRACK $trackstr AUDIO\n"
      content="${content}    INDEX 00 00:00:00\n"
      content="${content}    INDEX 01 00:02:00"
    fi

    # Check if more lines will be added
    if [ "$trackNumber" -ne "${#gameBinArray[@]}" ]; then
      content="${content}\n"
      ((trackNumber++))
    fi
  done
  # fGenerate cueFile content

  # Declare final cueFile path
  cueFilePath="$gamePath/$gameName.cue"

  # Delete the cue file if already exists
  printf "\nRemoving previous '$cueFilePath' if it exists\n"
  rm -f "$cueFilePath"

  # Print cueFile content to check is correct
  printf "\nGenerated '$cueFilePath' with content:\n"
  printf "$content\n"

  # Save the content in cueFile
  printf "$content" > "$cueFilePath"
}


# ------------------------------------------------------------------------------
# Main Script
# ------------------------------------------------------------------------------

# Clean psx folder if it exists
printf "Removing previous '$outputFolder' folder if exists\n"
rm -rf "$outputFolder"

# get all the 7z from the folder
for game7z in "$source7zFolder"/*.7z; do
  gameName=$(basename "$game7z" ".7z")
  outputGamePath=$"$outputFolder/$gameName"

  printf "\n##\n## Starting to extract and generate cue files for '$gameName'\n"

  printf "\n##\n## UnZip '$game7z' game\n"
  unZipGame "$game7z" "$outputGamePath"

  printf "\n##\n## Generating '.cue' file for '$gameName'\n"
  generateCueFile "$gameName" "$outputGamePath"
done

# ------------------------------------------------------------------------------
# Script Finished
trap : 0
printf >&2 "\n##\n## Extraction done!\n"